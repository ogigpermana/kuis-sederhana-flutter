import 'package:flutter/material.dart';
import './quiz_page.dart';

class LandingPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new Material(
      color: Colors.cyan,
      child: new InkWell(
        onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new QuizPage())),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text("Kuis Tebak", style: new TextStyle(color: Colors.greenAccent, fontSize: 40.0, fontWeight: FontWeight.bold),),
            new Text("Sentuh layar untuk memulainya!", style: new TextStyle(color: Colors.white, fontSize: 22.0, fontWeight: FontWeight.bold),),
            new Text("v.0.0.1 - by Ogi G Permana", style: new TextStyle(color: Colors.yellowAccent, fontSize: 10.0, fontWeight: FontWeight.bold),),
          ],
        ),
      ),
    );
  }
}