import 'package:flutter/material.dart';

import '../utils/question.dart';
import '../utils/quiz.dart';

import '../UI/answer_button.dart';
import '../UI/question_text.dart';
import '../UI/correct_wrong_overlay.dart';

import './score_page.dart';

class QuizPage extends StatefulWidget{
  @override
  State createState() => new QuizPageState();
}

class QuizPageState extends State<QuizPage>{

  Question currentQuestion;
  Quiz quiz = new Quiz([
    new Question("Ibukota negara Indonesia adalah Jakarta", true),
    new Question("Harimau berkembang biak dengan melahirkan", true),
    new Question("Satu minggu terdiri dari tujuh hari", true),
    new Question("Matahari terbenam dari sebelah barat", true),
    new Question("Mata uang negara india adalah rupee", true),
    new Question("Kubus memiliki 12 rusuk", true),
    new Question("Presiden pertama RI adalah Ir. Soekarno", true),
    new Question("Hasil dari 20 dibagi 4 adalah 5", true),
    new Question("Salah satu transportasi udara adalah pesawat terbang", true),
    new Question("Ikan Mas bernafas dengan insang", true),
    new Question("Ibukota negara filipina adalah Kuala Lumpur", false),
    new Question("Semua bilangan ganjil habis dibagi dua", false),
    new Question("Ayam berkembang biak dengan melahirkan", false),
    new Question("Sidoarjo adalah salah satu kota di jawa barat", false),
    new Question("Matahari terbit di sebelah utara", false),
    new Question("Ayam memiliki 4 kaki", false),
    new Question("Hasil dari 3 pangkat 2 adalah 6", false),
    new Question("Indonesia memiliki 4 musim", false),
    new Question("Bulan adalah salah satu nama planet", false),
    new Question("1 jam sama dengan 360 detik", false),
  ]);
  String questionText;
  int questionNumber;
  bool isCorrect;
  bool overlayShouldBeVisible = false;

  @override
  void initState() {
    super.initState();
    currentQuestion = quiz.nextQuestion;
    questionText = currentQuestion.question;
    questionNumber = quiz.questionNumber;
  }

  void handlerAnswer(bool answer){
    isCorrect = (currentQuestion.answer == answer);
    quiz.answer(isCorrect);
    this.setState(() {
      overlayShouldBeVisible = true;
    });
  }

  @override
  Widget build(BuildContext context){
    return new Stack(
      fit: StackFit.expand,
      children: <Widget>[
        new Column(
          // This is our main page
          children: <Widget>[
            new AnswerButton(true, () => handlerAnswer(true)), // True button
            new QuestionText(questionText, questionNumber),
            new AnswerButton(false, () => handlerAnswer(false))
          ],
        ),
        overlayShouldBeVisible == true ? new CorrectWrongOverlay(
          isCorrect,
            () {
              if(quiz.length == questionNumber){
                Navigator.of(context).pushAndRemoveUntil(new MaterialPageRoute(builder: (BuildContext context) => new ScorePage(quiz.score, quiz.length)), (Route route) => route == null);
                return;
              }
              currentQuestion = quiz.nextQuestion;
              this.setState(() {
                overlayShouldBeVisible = false;
                questionText = currentQuestion.question;
                questionNumber = quiz.questionNumber;
              });
            }
        ) : new Container()
      ],
    );
  }
}